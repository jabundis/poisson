import numpy as np
import scipy.sparse as sparse
import scipy.sparse.linalg

import scipy.constants as cons

perm_vacuum = cons.epsilon_0 #Global constant

def potential(eps, rho, V_left=0, V_right=0):
    """
    Solves Poisson Eq.:

        div(eps * grad(V)) = -rho/perm_vacuum,

    using central differences.

    Retrieves potential "V" (normalized) given: the dielectric 
    constants "eps", volume charge densities "rho" at a grid 
    of constant spacing (z(j) = z(0)+j*Delta_z for all j), 
    and potential values (normalized) at the grid 
    edges "V_left" and "V_right". By default considers "V_left" 
    and "V_right" equal to zero.
    
    Averages neighboring values of "eps" as in paper of 
    Prof. Jirauschek.

    Input:
        eps (1d ndarray) -> Diel. Const. (unit-less)
        rho (1d ndarray) -> Vol. charge density (any units!)
        V_left (float) -> V(z(0)) * factor (unit-less)
        V_right (float) -> V(z(N)) * factor (unit-less)

    Output:
        V_norm (1d ndarray) -> Normalized potential at grid points (unitless). 

            V_norm = V * factor
            factor = perm_vacuum/(Delta_z**2 * max(abs(rho)))
    """
    M_above = eps[1:-2] + eps[2:-1] 
    M_diag  = - (eps[:-2] + 2*eps[1:-1] + eps[2:])
    M_below = M_above

    n = eps.size - 2 #Number of unknowns 
    M = sparse.diags((M_above, M_diag, M_below),(1,0,-1),shape=(n,n),format='csr' ) 

    normalizedRho = 2*rho / np.max(np.abs(rho))
    L = - normalizedRho[1:-1]
    L[0] = L[0] - V_left*(eps[0]+eps[1])
    L[-1] = L[-1] - V_right*(eps[-2]+eps[-1])
    
    V_norm = sparse.linalg.spsolve(M, L)
    return V_norm


def potential_noAvg(eps, rho, V_left=0, V_right=0): 
    """
    As function "potential" except neighbor dielectric constants 
    are not averaged. Good for use in multiple dielectric slabs with 
    doping, whereas each is described by constant permittivity.
    """
    M_above = eps[1:-2]
    M_diag  = - ( eps[:-2] + eps[1:-1] )
    M_below = M_above

    n = eps.size - 2 #Number of unknowns 
    M = sparse.diags((M_above, M_diag, M_below),(1,0,-1),shape=(n,n),format='csr' ) 

    normalizedRho = rho / np.max(np.abs(rho))
    L = - normalizedRho[1:-1]
    L[0] = L[0] - V_left*eps[0]
    L[-1] = L[-1] - V_right*eps[-1]
    
    V_norm = sparse.linalg.spsolve(M, L)
    return V_norm


def potential_unnormalized(eps, rho, Delta_z, V_left=0, V_right=0):
    """
    As function "potential" except no normalization takes place.

    Input:
        eps (1d ndarray) -> Diel. Const. (unit-less)
        rho (1d ndarray) -> Vol. charge density (C/cm**(3))
        Delta_z (float) -> Grid constant spacing (nm)
        V_left (float) -> V(z(0)) (Volt)
        V_right (float) -> V(z(N)) (Volt)

    Output:
        V (1d ndarray) -> Potential at grid points (Volt). 
    """
    M_above = eps[1:-2] + eps[2:-1] 
    M_diag  = - (eps[:-2] + 2*eps[1:-1] + eps[2:])
    M_below = M_above

    n = eps.size - 2 #Number of unknowns 
    M = sparse.diags((M_above, M_diag, M_below),(1,0,-1),shape=(n,n),format='csr' ) 

    #Turn into SI units
    rho = rho * 1e6
    Delta_z = Delta_z * 1e-9

    normalizedRho = 2*rho*Delta_z**2 / perm_vacuum
    L = - normalizedRho[1:-1]
    L[0] = L[0] - V_left*(eps[0]+eps[1])
    L[-1] = L[-1] - V_right*(eps[-2]+eps[-1])
    
    V = sparse.linalg.spsolve(M, L)
    return V
