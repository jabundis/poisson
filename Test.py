import numpy as np
import Poisson_finiteDif

import scipy.constants as cons


def test_1():
    """
    Test Poisson_finiteDif.potential
    """
    eps = np.array([3.8,2,6,1,2.2,3.7])
    rho = np.array([-3, -1, -2, -5, -1, -4])

    V_norm = Poisson_finiteDif.potential(eps, rho)
    return V_norm

def test_2():
    """
    Test Poisson_finiteDif.potential_noAvg
    """
    eps = np.array([3.8,2,6,1,2.2,3.7])
    rho = np.array([-3, -1, -2, -5, -1, -4])

    V_norm = Poisson_finiteDif.potential_noAvg(eps, rho)
    return V_norm

def test_3():
    """
    Test Poisson_finiteDif.potential_unnormalized
    """
    Nd = 1e17 #Doping in cm**(-3)

    eps = np.array([3.8,2,6,1,2.2,3.7]) #unitless
    rho = np.array([-3, -1, -2, -5, -1, -4]) * cons.e * Nd #C/cm**3
    Delta_z = 1 #nm

    V = Poisson_finiteDif.potential_unnormalized(eps, rho, Delta_z)
    return V

