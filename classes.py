class Poisson():
    def __init__(self, solver='FiniteDifferences'):
        self.solver=solver
        self.rho = None

    def get_rho(self):
        return self.rho
    def get_method(self):
        return self.method

    def set_solver(self, solver):
        self.solver = solver
    def set_rho(self, rho):
        self.rho = rho

    def solve(self):
        pass
    def plot(self, z=None):
        pass

class Solver():
    def __init__(self, eps, rho):
        self.eps = eps
        self.rho = rho
    def get_eps(self):
        self.eps
    def get_rho(self):
        self.rho
    def set_eps(self, eps):
        self.eps = eps
    def set_rho(self, rho):
        self.rho = rho
    def solve(self):
        pass

class FiniteDifferences(Solver):
    def __init__(self, h, eps, rho):
        self.h = h      #Constant spacing (float)
        self.eps = eps  #Diel. constants instance
        self.rho = rho  #Volume charge density instance
    def get_h(self):
        return self.h
    def set_h(self, h):
        self.h = h
